# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:
git clone https://europemaster@bitbucket.org/europemaster/stroboskop.git

Naloga 6.2.3:
https://bitbucket.org/europemaster/stroboskop/commits/0abbfcdf3ce8829bdb9fdc7b1f07073e59724486

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/europemaster/stroboskop/commits/b356ce362329aea22ea33e64959899f6bdac9138

Naloga 6.3.2:
https://bitbucket.org/europemaster/stroboskop/commits/6542214b82b522fbe7ad1d39c6b5820ab911d374

Naloga 6.3.3:
https://bitbucket.org/europemaster/stroboskop/commits/df9aacfcd86939ae1b8f615b01c79c6c42fb2d07

Naloga 6.3.4:
https://bitbucket.org/europemaster/stroboskop/commits/1d43f17a9a7c6b1e7fa6c7db0ce215fed9a77d27

Naloga 6.3.5:

git checkout master
git pull origin master
git merge izgled
git add .
git commit -m "merge commit"
git push origin master

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/europemaster/stroboskop/commits/962e5d7b90b31522dff7119c524c9396a2b532a2

Naloga 6.4.2:
https://bitbucket.org/europemaster/stroboskop/commits/92301f44799c6db690d6fb611de02750ad08fc73

Naloga 6.4.3:
https://bitbucket.org/europemaster/stroboskop/commits/13f1e951d1efc5b1651e4a588ce57d72fcdab5d1

Naloga 6.4.4:
https://bitbucket.org/europemaster/stroboskop/commits/5fc10e58181df58cb06a3b58a0f13cb444f1df22